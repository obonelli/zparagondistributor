sap.ui.define(
  [
    'sap/ui/core/mvc/Controller',
    'sap/m/MessageBox',
    './utilities',
    'sap/ui/core/routing/History',
    'sap/ui/model/json/JSONModel',
  ],
  function (BaseController, MessageBox, Utilities, History, JSONModel) {
    'use strict';

    return BaseController.extend(
      'com.sap.build.standard.zobx_p28_kitsd.controller.Page2',
      {
        getSearchFilter: function () {
          let $filter = '';

          // Obtener filtros actuales y filtrar
          let selectedMatItems = this.getView()
            .byId('filterMaterial')
            .getSelectedItems();
          let selectedMaterials = selectedMatItems.map((e) => e.getText());

          let selectedCompMatItems = this.getView()
            .byId('filterComponentMaterial')
            .getSelectedItems();
          let selectedCompMaterials = selectedCompMatItems.map((e) =>
            e.getText()
          );

          let selectedComponentBatchItems = this.getView()
            .byId('filterComponentBatch')
            .getSelectedItems();
          let selectedComponentBatch = selectedComponentBatchItems.map((e) => e.getText());

          let tableData;

          // if (selectedMaterials.length || selectedCompMaterials.length || selectedComponentBatchItems.length) {
          //   tableData = data.filter((e) => {
          //     return (
          //       selectedMaterials.includes(e.Material) ||
          //       selectedCompMaterials.includes(e.ComponentMaterial) ||
          //       selectedComponentBatch.includes(e.ComponentBatch)
          //     );
          //   });
          // } else {
          //   tableData = data;
          // }

          var filters = [];
          var ComponentMaterialFilter = [];
          var ComponentBatchFilter = [];
          //debugger
          if (this.getView().byId("filterComponentMaterial").getSelectedKeys() != "") {
            //console.log('Aqui paso');
            //console.log()
            for (var valor of selectedCompMaterials) {
              //console.log(valor);
              ComponentMaterialFilter.push(
                `ComponentMaterial eq '${valor}'`
              )
            }
            ComponentMaterialFilter = [... new Set(ComponentMaterialFilter)].join(" or ");
            filters.push(`(
              ${ComponentMaterialFilter}
              )`)
          }
          if (this.getView().byId("filterComponentBatch").getSelectedKeys() != "") {
            for (var valor of selectedComponentBatch) {
              //console.log(valor);
              ComponentBatchFilter.push(
                `ComponentBatch eq '${valor}'`)
            }
            ComponentBatchFilter = [... new Set(ComponentBatchFilter)].join(" or ");
            filters.push(`(
              ${ComponentBatchFilter})`)
          }
          //debugger
          //if (filters.length !== 0) {
          //return filters.join(" and ");
          /*} else {
            tableData = data;
          }*/

          //this.getView().byId('filterMaterial').getS
          let { KitPlant, KitSlocation, Material, KitMaterial, KitSerial, KitBatch, Batch, Serial } = this.getOwnerComponent()
            .getModel('items')
            .getData();

          //$filter = `$filter=KitPlant eq '${Plant}' and KitSlocation eq '${Slocation}' and KitMaterial eq '${Material}'and KitBatch eq '${Batch}' and KitSerial eq '${Serial}' and ComponentMaterial eq '${ComponentMaterialFilter}' and ComponentBatch eq '${ComponentBatchFilter}'`;

          if (ComponentMaterialFilter.length !== 0 && ComponentBatchFilter.length !== 0) {
            $filter = `$filter=KitPlant eq '${KitPlant}' and KitSlocation eq '${KitSlocation}' and KitMaterial eq '${KitMaterial}'and KitBatch eq '${KitBatch}' and KitSerial eq '${KitSerial}' and (${ComponentMaterialFilter}) and (${ComponentBatchFilter})`;
          } else if (ComponentMaterialFilter.length !== 0) {
            $filter = `$filter=KitPlant eq '${KitPlant}' and KitSlocation eq '${KitSlocation}' and KitMaterial eq '${KitMaterial}'and KitBatch eq '${KitBatch}' and KitSerial eq '${KitSerial}' and (${ComponentMaterialFilter})`;
          } else if (ComponentBatchFilter.length !== 0) {
            $filter = `$filter=KitPlant eq '${KitPlant}' and KitSlocation eq '${KitSlocation}' and KitMaterial eq '${KitMaterial}'and KitBatch eq '${KitBatch}' and KitSerial eq '${KitSerial}' and (${ComponentBatchFilter})`;
          } else {
            $filter = `$filter=KitPlant eq '${KitPlant}' and KitSlocation eq '${KitSlocation}' and KitMaterial eq '${KitMaterial}'and KitBatch eq '${KitBatch}' and KitSerial eq '${KitSerial}'`;
          }

          return $filter;
        },

        populateLocalFilters: function (data) {
          // Initialize filters arrays
          for (let filter of this.filters) {
            filter = [];
          }

          // Populate filters arrays
          for (let item of data) {
            for (let filterName in this.filters) {
              let filter = this.filters[filterName];
              let val = item[filterName];
              if (!filter.includes(val)) {
                filter.push(val);
              }
            }
          }

          // Update filters models
          for (let filterName in this.filters) {
            let filterData = this.filters[filterName].map((e) => {
              return {
                value: e,
              };
            });
            this.getView()
              .byId(`filter${filterName}`)
              .setModel(
                new JSONModel({
                  FilterSet: filterData,
                })
              );
          }
        },

        onSearch: function () {
          sap.ui.core.BusyIndicator.show();
          //debugger
          let $filter = this.getSearchFilter();
          var url = `${this.serviceUrl}/ZACV_C_KIT_ITEM?${$filter}`;
          $.ajax({
            type: 'GET',
            url: url,
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Accept', 'application/json');
            },
            context: this,
            dataType: 'json',
            success: function (res) {
              let item = this.getOwnerComponent().getModel('items').getData();
              console.log(item);
              //console.log(res);
              let data = res.d.results.map((e) => {
                e.KitPlant = item.KitPlant;
                e.KitSlocation = item.KitSlocation;
                e.KitBatch = item.KitBatch;
                e.ComponentStatus = item.ComponentStatus;
                e.KitSerial = item.KitSerial;
                e.ComponentMaterial = item.ComponentMaterial;
                e.ComponentMaterialDesc = item.ComponentMaterialDesc;
                e.KitSerial = item.KitSerial;
                e.KitBatch;
                e.KitMaterial = item.KitMaterial;
                // e.ComponentBatch = item.ComponentBatch;
                return e;
              });

              /**/

              // Aplanar result
              /*for (let i in res.d.results) {
                let el = res.d.results[i];
                if (!data.length) {
                  data.push(el);
                } else {
                  let found = false;
                  for (let it of data) {
                    if (JSON.stringify(it) === JSON.stringify(el)) {
                      found = true;
                      break;
                    }
                  }
                  if (!found) {
                    data.push(el);
                  }
                }
              }*/

              /**/

              var tableData = data;

              const aModel = new sap.ui.model.json.JSONModel({
                SalesOrderSet: tableData,
              });
              aModel.setSizeLimit(tableData.length);

              this.getView().byId('tableItems').setModel(aModel);
              //this.populateLocalFilters(data);
              sap.ui.core.BusyIndicator.hide();

              /* */
              function onlyUnique(value, index, self) {
                return self.indexOf(value) === index;
              }

              //
              let compMaterials = data
                .map((e) => {
                  return e.ComponentMaterial;
                })
                .filter(onlyUnique)
                .map((e, i) => {
                  return {
                    key: i,
                    value: e,
                  };
                });
              this.getView()
                .byId('filterComponentMaterial')
                .setModel(
                  new sap.ui.model.json.JSONModel({
                    FilterSet: compMaterials,
                  })
                );

              let compBatch = data
                .map((e) => {
                  return e.ComponentBatch;
                })
                .filter(onlyUnique)
                .map((e, i) => {
                  return {
                    key: i,
                    value: e,
                  };
                });
              this.getView()
                .byId('filterComponentBatch')
                .setModel(
                  new sap.ui.model.json.JSONModel({
                    FilterSet: compBatch,
                  })
                );

              /*** */
            },
            error: function (err) {
              sap.ui.core.BusyIndicator.hide();
              const details =
                (err && err.message && err.message.value) ||
                'Error not defined.';
              MessageBox.error('Error', {
                title: 'Error found searching',
                details,
              });
            },
          });
        },

        formatLeadingZeros: function (n) {
          return n?.replace(/^0+/, '') || '';
        },

        formatDate: function (date) {
          let formattedFechaEntrega = '';
          if (date) {
            const formatter = sap.ui.core.format.DateFormat.getDateInstance({
              pattern: 'dd/MM/YYYY',
            });
            formattedFechaEntrega = formatter.format(new Date(date));
          }
          return formattedFechaEntrega;
        },

        handleRouteMatched: function (oEvent) {
          /*
          var sAppId = 'App60a2a3ba0a52d66331ca971e';

          var oParams = {};

          if (oEvent.mParameters.data.context) {
            this.sContext = oEvent.mParameters.data.context;
          } else {
            if (this.getOwnerComponent().getComponentData()) {
              var patternConvert = function (oParam) {
                if (Object.keys(oParam).length !== 0) {
                  for (var prop in oParam) {
                    if (prop !== 'sourcePrototype' && prop.includes('Set')) {
                      return prop + '(' + oParam[prop][0] + ')';
                    }
                  }
                }
              };

              this.sContext = patternConvert(
                this.getOwnerComponent().getComponentData().startupParameters
              );
            }
          }

          var oPath;

          if (this.sContext) {
            oPath = {
              path: '/' + this.sContext,
              parameters: oParams,
            };
            this.getView().bindObject(oPath);
          }
          */
          this.onSearch();
        },
        _onFioriListReportTableUpdateFinished: function (oEvent) {
          var oTable = oEvent.getSource();
          var oHeaderbar = oTable.getAggregation('headerToolbar');
          if (oHeaderbar && oHeaderbar.getAggregation('content')[1]) {
            var oTitle = oHeaderbar.getAggregation('content')[1];
            if (
              oTable.getBinding('items') &&
              oTable.getBinding('items').isLengthFinal()
            ) {
              oTitle.setText(
                '(' + oTable.getBinding('items').getLength() + ')'
              );
            } else {
              oTitle.setText('(1)');
            }
          }
        },
        onInit: function () {
          this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
          this.oRouter
            .getTarget('Page2')
            .attachDisplay(jQuery.proxy(this.handleRouteMatched, this));

          this.serviceUrl = `/sap/opu/odata/sap/ZACV_C_KIT_ITEM_CDS`;

          this.filters = {
            Material: [],
            Batch: [],
            Serial: [],
            Plant: [],
            Slocation: [],
            Status: [],
            ComponentMaterial: [],
            ComponentBatch: [],
            KitSerial: [],
            KitBatch: []
          };
        },
        onExit: function () {
          // to destroy templates for bound aggregations when templateShareable is true on exit to prevent duplicateId issue
          var aControls = [
            {
              controlId:
                'Fiori_ListReport_ListReport_0-content-Fiori_ListReport_Table-1',
              groups: ['items'],
            },
          ];
          for (var i = 0; i < aControls.length; i++) {
            var oControl = this.getView().byId(aControls[i].controlId);
            if (oControl) {
              for (var j = 0; j < aControls[i].groups.length; j++) {
                var sAggregationName = aControls[i].groups[j];
                var oBindingInfo = oControl.getBindingInfo(sAggregationName);
                if (oBindingInfo) {
                  var oTemplate = oBindingInfo.template;
                  oTemplate.destroy();
                }
              }
            }
          }
        },
      }
    );
  },
  /* bExport= */ true
);
