sap.ui.define(
  [
    'sap/ui/core/mvc/Controller',
    'sap/m/MessageBox',
    './utilities',
    'sap/ui/core/routing/History',
    'sap/ui/model/json/JSONModel',
  ],
  function (BaseController, MessageBox, Utilities, History, JSONModel) {
    'use strict';

    return BaseController.extend(
      'com.sap.build.standard.zobx_p28_kitsd.controller.Page1',
      {
        getSearchFilter: function () {
          let $filter = '';

          // Obtener filtros actuales y filtrar
          let selectedMaterialsItems = this.getView()
            .byId('filterMaterial')
            .getSelectedItems();
          let selectedMaterials = selectedMaterialsItems.map((e) =>
            e.getText()
          );
          //debugger

          let selectedBatchItems = this.getView()
            .byId('filterBatch')
            .getSelectedItems();
          let selectedBatchs = selectedBatchItems.map((e) => e.getText());

          let selectedPlantItems = this.getView()
            .byId('filterPlant')
            .getSelectedItems();
          let selectedPlants = selectedPlantItems.map((e) => e.getText());

          let selectedStorageLocationItems = this.getView()
            .byId('filterSlocation')
            .getSelectedItems();
          let selectedStorageLocation = selectedStorageLocationItems.map((e) => e.getText());

          let selectedStatusItems = this.getView()
            .byId('filterStatus')
            .getSelectedItems();
          let selectedStatus = selectedStatusItems.map((e) => e.getText());

          let selectedComponentMaterialItems = this.getView()
            .byId('filterComponentMaterial')
            .getSelectedItems();
          let selectedComponentMaterial = selectedComponentMaterialItems.map((e) => e.getText());

          let selectedComponentBatchItems = this.getView()
            .byId('filterComponentBatch')
            .getSelectedItems();
          let selectedComponentBatch = selectedComponentBatchItems.map((e) => e.getText());

          let selectedComponentSerialNumberItems = this.getView()
            .byId('filterComponentSerialNumber')
            .getSelectedItems();
          let selectedComponentSerialNumber = selectedComponentSerialNumberItems.map((e) => e.getText());

          // let tableData;

          // if (selectedMaterials.length || selectedCompMaterials.length || selectedBatchItems.length
          //   || selectedPlantItems.length || selectedStorageLocationItems.length || selectedStatusItems.length) {
          //   tableData = data.filter((e) => {
          //     return (
          //       selectedMaterials.includes(e.Material) ||
          //       selectedBatchs.includes(e.Batch) ||
          //       selectedPlants.includes(e.Plant) ||
          //       selectedStorageLocation.includes(e.Slocation) ||
          //       selectedStatus.includes(e.Status)
          //     );
          //   });
          // } else {
          //   tableData = data;
          // }

          var filters = [];
          var SelectedMat = [];
          var SelectedBatch = [];
          var SelectedPlant = [];
          var SelectedSlocation = [];
          var SelectedStatus = [];
          var SelectedComponentMaterial = [];
          var SelectedComponentBatch = [];
          var SelectedComponentSerial = [];

          //debugger
          if (this.getView().byId("filterMaterial").getSelectedKeys() != "") {
            //console.log('Aqui paso');
            //console.log()
            for (var valor of selectedMaterials) {
              //console.log(valor);
              SelectedMat.push(
                `KitMaterial eq '${valor}'`)
            }
            SelectedMat = [... new Set(SelectedMat)].join(" or ");
            filters.push(`(${SelectedMat})`)
          }
          if (this.getView().byId("filterBatch").getSelectedKeys() != "") {
            for (var valor of selectedBatchs) {
              SelectedBatch.push(
                `KitBatch eq '${valor}'`)
            }
            SelectedBatch = [... new Set(SelectedBatch)].join(" or ");
            filters.push(`(${SelectedBatch})`)
          }
          if (this.getView().byId("filterPlant").getSelectedKeys() != "") {
            for (var valor of selectedPlants) {
              SelectedPlant.push(
                `KitPlant eq '${valor}'`
              )
            }
            SelectedPlant = [... new Set(SelectedPlant)].join(" or ");
            filters.push(`(${SelectedPlant})`)
          }
          if (this.getView().byId("filterSlocation").getSelectedKeys() != "") {
            for (var valor of selectedStorageLocation) {
              SelectedSlocation.push(
                `KitSlocation eq '${valor}'`
              )
            }
            SelectedSlocation = [... new Set(SelectedSlocation)].join(" or ");
            filters.push(
              `(${SelectedSlocation})`
            )
          }
          if (this.getView().byId("filterStatus").getSelectedKeys() != "") {
            for (var valor of selectedStatus) {
              SelectedStatus.push(
                `ComponentStatus eq '${valor}'`
              )
            }
            SelectedStatus = [... new Set(SelectedStatus)].join(" or ");
            filters.push(
              `(${SelectedStatus})`
            )
          }
          if (this.getView().byId("filterComponentMaterial").getSelectedKeys() != "") {
            for (var valor of selectedComponentMaterial) {
              SelectedComponentMaterial.push(
                `ComponentMaterial eq '${valor}'`
              )
            }
            SelectedComponentMaterial = [... new Set(SelectedComponentMaterial)].join(" or ");
            filters.push(
              `(${SelectedComponentMaterial})`
            )
          }
          if (this.getView().byId("filterComponentBatch").getSelectedKeys() != "") {
            for (var valor of selectedComponentBatch) {
              SelectedComponentBatch.push(
                `ComponentBatch eq '${valor}'`
              )
            }
            SelectedComponentBatch = [... new Set(SelectedComponentBatch)].join(" or ");
            filters.push(
              `(${SelectedComponentBatch})`
            )
          }
          if (this.getView().byId("filterComponentSerialNumber").getSelectedKeys() != "") {
            for (var valor of selectedComponentSerialNumber) {
              SelectedComponentSerial.push(
                `ComponentSerial eq '${valor}'`
              )
            }
            SelectedComponentSerial = [... new Set(SelectedComponentSerial)].join(" or ");
            filters.push(
              `(${SelectedComponentSerial})`
            )
          }
          //debugger
          if (filters.length !== 0) {
            return filters.join(" and ");
          } else {
            // tableData = data;
          }

          //return $filter;
        },

        onTableItemPress: function (oEvent) {
          debugger;
          let model = this.getOwnerComponent().getModel('items');
          if (!model) {
            this.getOwnerComponent().setModel(
              new JSONModel({
                Werks: '',
                Lgort: '',
                KitMatnr: '',
              }),
              'items'
            );
            model = this.getOwnerComponent().getModel('items');
          }

          let item = oEvent
            .getParameter('listItem')
            .getBindingContext()
            .getObject();
          model.setData(item);
          this.oRouter.navTo('Page2', {
            context: Math.random(),
          });
        },

        populateLocalFilters: function (data) {
          // Initialize filters arrays
          for (let filter of this.filters) {
            filter = [];
          }

          // Populate filters arrays
          for (let item of data) {
            for (let filterName in this.filters) {
              let filter = this.filters[filterName];
              let val = item[filterName];
              if (!filter.includes(val)) {
                filter.push(val);
              }
            }
          }

          // Update filters models
          for (let filterName in this.filters) {
            let filterData = this.filters[filterName].map((e) => {
              return {
                value: e,
              };
            });
            this.getView()
              .byId(`filter${filterName}`)
              .setModel(
                new JSONModel({
                  FilterSet: filterData,
                })
              );
          }
        },

        onSearch: function () {
          sap.ui.core.BusyIndicator.show();
          let $filter = this.getSearchFilter();
          console.log($filter);
          var url = `${this.serviceUrl}/ZACV_C_KIT_HEADER?$filter=${$filter}`;
          if ($filter) {
            var urlItemsSearch = `${this.serviceUrlItems}/ZACV_C_KIT_ITEM?$filter=${$filter}&$top=6`;
          } else {
            var urlItemsSearch = `${this.serviceUrlItems}/ZACV_C_KIT_ITEM?$top=30`;
          }
          $.ajax({
            type: 'GET',
            url: urlItemsSearch,
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Accept', 'application/json');
            },
            context: this,
            dataType: 'json',
            success: function (res) {
              //let data = [];

              var data = res.d.results;
              console.log(data);
              //var temps = data;
              //Status A = Active P = Partial I = Inactive
              for (const [key, value] of Object.entries(data)) {
                if (value.ComponentStatus == 'A') {
                  value.ComponentStatus = 'Active';
                }
                if (value.ComponentStatus == 'P') {
                  value.ComponentStatus = 'Partial';
                }
                if (value.ComponentStatus == 'I') {
                  value.ComponentStatus = 'Inactive';
                }
              }
              //console.log(data);

              /**/

              //Aplanar result
              /*for (let i in res.d.results) {
                let el = res.d.results[i];
                if (!data.length) {
                  data.push(el);
                } else {
                  let found = false;
                  for (let it of data) {
                    if (JSON.stringify(it) === JSON.stringify(el)) {
                      found = true;
                      break;
                    }
                  }
                  if (!found) {
                    data.push(el);
                  }
                }
              }*/

              var tableData = data;


              /**/

              const aModel = new sap.ui.model.json.JSONModel({
                SalesOrderSet: tableData,
              });
              aModel.setSizeLimit(tableData.length);

              this.getView().byId('tableItems').setModel(aModel);
              //this.populateLocalFilters(data);
              sap.ui.core.BusyIndicator.hide();
            },
            error: function (err) {
              sap.ui.core.BusyIndicator.hide();
              const details =
                (err && err.message && err.message.value) ||
                'Error not defined.';
              MessageBox.error('Error', {
                title: 'Error found searching',
                details,
              });
            },
          });
        },

        formatLeadingZeros: function (n) {
          return n?.replace(/^0+/, '') || '';
        },

        formatDate: function (date) {
          let formattedFechaEntrega = '';
          if (date) {
            const formatter = sap.ui.core.format.DateFormat.getDateInstance({
              pattern: 'dd/MM/YYYY',
            });
            formattedFechaEntrega = formatter.format(new Date(date));
          }
          return formattedFechaEntrega;
        },

        handleRouteMatched: function (oEvent) {
          /*
          var sAppId = 'App60a2a3ba0a52d66331ca971e';

          var oParams = {};

          if (oEvent.mParameters.data.context) {
            this.sContext = oEvent.mParameters.data.context;
          } else {
            if (this.getOwnerComponent().getComponentData()) {
              var patternConvert = function (oParam) {
                if (Object.keys(oParam).length !== 0) {
                  for (var prop in oParam) {
                    if (prop !== 'sourcePrototype' && prop.includes('Set')) {
                      return prop + '(' + oParam[prop][0] + ')';
                    }
                  }
                }
              };

              this.sContext = patternConvert(
                this.getOwnerComponent().getComponentData().startupParameters
              );
            }
          }

          var oPath;

          if (this.sContext) {
            oPath = {
              path: '/' + this.sContext,
              parameters: oParams,
            };
            this.getView().bindObject(oPath);
          }
          */
          //this.onSearch();
        },
        _onFioriListReportTableUpdateFinished: function (oEvent) {
          var oTable = oEvent.getSource();
          var oHeaderbar = oTable.getAggregation('headerToolbar');
          if (oHeaderbar && oHeaderbar.getAggregation('content')[1]) {
            var oTitle = oHeaderbar.getAggregation('content')[1];
            if (
              oTable.getBinding('items') &&
              oTable.getBinding('items').isLengthFinal()
            ) {
              oTitle.setText(
                '(' + oTable.getBinding('items').getLength() + ')'
              );
            } else {
              oTitle.setText('(1)');
            }
          }
        },
        onInit: function () {
          this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
          this.oRouter
            .getTarget('Page1')
            .attachDisplay(jQuery.proxy(this.handleRouteMatched, this));

          this.serviceUrl = `/sap/opu/odata/sap/ZACV_C_KIT_HEADER_CDS`;
          this.serviceUrlItems = `/sap/opu/odata/sap/ZACV_C_KIT_ITEM_CDS`;
          var url = `${this.serviceUrl}/ZACV_C_KIT_HEADER`;
          var urlMaterial = `${this.serviceUrl}/I_Material`;

          this.filters = {
            Material: [],
            Batch: [],
            Plant: [],
            Slocation: [],
            Status: [],
            ComponentMaterial: [],
            ComponentBatch: [],
            ComponentSerialNumber: [],
            data: [],
            dataItems: [],
            status: [],
          };

          $.ajax({
            type: 'GET',
            url: url,
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Accept', 'application/json');
            },
            context: this,
            dataType: 'json',
            success: function (res) {

              this.filters.data = res.d.results;
              this.filters.dataItems = this.filters.data;
              //console.log(this.filters.data)

              var PlantItemFilter = [];
              var SlocationItemFilter = null;
              var MaterialBatchFilter = null


              for (const [key, value] of Object.entries(this.filters.dataItems)) {
                //console.log(value.Plant)
                PlantItemFilter.push(value.Plant)
                // SlocationItemFilter.push(value.Slocation)
                // MaterialBatchFilter.push(value.Material)

                // PlantItemFilter = value.Plant
                SlocationItemFilter = value.Slocation
                MaterialBatchFilter = value.Material
              }

              PlantItemFilter = [... new Set(PlantItemFilter)].join(" ");
              // SlocationItemFilter = [... new Set(SlocationItemFilter)].join(" ");
              // MaterialBatchFilter = [... new Set(MaterialBatchFilter)].join(" ");
              //and KitSlocation eq '${SlocationItemFilter}' and KitMaterial eq '${MaterialBatchFilter}'
              var filterItem = `$filter=KitPlant eq '${PlantItemFilter}'`;
              var urlItems = `${this.serviceUrlItems}/ZACV_C_KIT_ITEM?${filterItem}`;

              $.ajax({
                type: 'GET',
                url: urlItems,
                beforeSend: function (xhr) {
                  xhr.setRequestHeader('Accept', 'application/json');
                },
                context: this,
                dataType: 'json',
                success: function (res) {

                  let dataItems = res.d.results;
                  //console.log(dataItems);
                  //Se llenan los combos al iniciar la app
                  function onlyUnique(value, index, self) {
                    return self.indexOf(value) === index;
                  }

                  for (const [key, value] of Object.entries(dataItems)) {
                    //console.log(key);
                    //console.log(value);
                    if (value.ComponentStatus == 'A') {
                      value.ComponentStatus = 'Active';
                    }
                    if (value.ComponentStatus == 'P') {
                      value.ComponentStatus = 'Partial';
                    }
                    if (value.ComponentStatus == 'I') {
                      value.ComponentStatus = 'Inactive';
                    }
                  }

                  let compMaterials = dataItems
                    .map((e) => {
                      return e.ComponentMaterial;
                    })
                    .filter(onlyUnique)
                    .map((e, i) => {
                      return {
                        key: i,
                        value: e,
                      };
                    });
                  this.getView()
                    .byId('filterComponentMaterial')
                    .setModel(
                      new sap.ui.model.json.JSONModel({
                        FilterSet: compMaterials,
                      })
                    );

                  let compBatch = dataItems
                    .map((e) => {
                      return e.ComponentBatch;
                    })
                    .filter(onlyUnique)
                    .map((e, i) => {
                      return {
                        key: i,
                        value: e,
                      };
                    });
                  this.getView()
                    .byId('filterComponentBatch')
                    .setModel(
                      new sap.ui.model.json.JSONModel({
                        FilterSet: compBatch,
                      })
                    );

                  let compSerial = dataItems
                    .map((e) => {
                      return e.ComponentSerial;
                    })
                    .filter(onlyUnique)
                    .map((e, i) => {
                      return {
                        key: i,
                        value: e,
                      };
                    });
                  this.getView()
                    .byId('filterComponentSerialNumber')
                    .setModel(
                      new sap.ui.model.json.JSONModel({
                        FilterSet: compSerial,
                      })
                    );

                  let status = dataItems
                    .map((e) => {
                      return e.ComponentStatus;
                    })
                    .filter(onlyUnique)
                    .map((e, i) => {
                      return {
                        key: i,
                        value: e,
                      };
                    });
                  this.getView()
                    .byId('filterStatus')
                    .setModel(
                      new sap.ui.model.json.JSONModel({
                        FilterSet: status,
                      })
                    );
                  ////

                },
                error: function (err) {
                  sap.ui.core.BusyIndicator.hide();
                  const details =
                    (err && err.message && err.message.value) ||
                    'Error not defined.';
                  MessageBox.error('Error', {
                    title: 'Error found searching',
                    details,
                  });
                },
              });


              for (const [key, value] of Object.entries(this.filters.data)) {
                //console.log(key);
                //console.log(value.Batch);
                if (value.Status == 'A') {
                  value.Status = 'Active';
                  //console.log(key, value);
                  //temps = key, value;
                  //console.log(value.Status);
                }
                if (value.Status == 'P') {
                  value.Status = 'Partial';
                }
                if (value.Status == 'I') {
                  value.Status = 'Inactive';
                }
              }
              //console.log(temps);
              //console.log(this.filters.data);
              //Se llenan los combos al iniciar la app
              function onlyUnique(value, index, self) {
                return self.indexOf(value) === index;
              }

              let batchnumbers = this.filters.data
                .map((e) => {
                  return e.Batch;
                })
                .filter(onlyUnique)
                .map((e, i) => {
                  return {
                    key: i,
                    value: e,
                  };
                });
              this.getView()
                .byId('filterBatch')
                .setModel(
                  new sap.ui.model.json.JSONModel({
                    FilterSet: batchnumbers,
                  })
                );

              let materials = this.filters.data
                .map((e) => {
                  return e.Material;
                })
                .filter(onlyUnique)
                .map((e, i) => {
                  return {
                    key: i,
                    value: e,
                  };
                });
              this.getView()
                .byId('filterMaterial')
                .setModel(
                  new sap.ui.model.json.JSONModel({
                    FilterSet: materials,
                  })
                );

              let plants = this.filters.data
                .map((e) => {
                  return e.Plant;
                })
                .filter(onlyUnique)
                .map((e, i) => {
                  return {
                    key: i,
                    value: e,
                  };
                });
              this.getView()
                .byId('filterPlant')
                .setModel(
                  new sap.ui.model.json.JSONModel({
                    FilterSet: plants,
                  })
                );

              let slocations = this.filters.data
                .map((e) => {
                  return e.Slocation;
                })
                .filter(onlyUnique)
                .map((e, i) => {
                  return {
                    key: i,
                    value: e,
                  };
                });
              this.getView()
                .byId('filterSlocation')
                .setModel(
                  new sap.ui.model.json.JSONModel({
                    FilterSet: slocations,
                  })
                );

              ////

            },
            error: function (err) {
              sap.ui.core.BusyIndicator.hide();
              const details =
                (err && err.message && err.message.value) ||
                'Error not defined.';
              MessageBox.error('Error', {
                title: 'Error found searching',
                details,
              });
            },
          });

          $.ajax({
            type: 'GET',
            url: urlMaterial,
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Accept', 'application/json');
            },
            context: this,
            dataType: 'json',
            success: function (res) {

              let dataMaterial = res.d.results;
              //console.log(dataMaterial);
              //Se llenan los combos al iniciar la app
              function onlyUnique(value, index, self) {
                return self.indexOf(value) === index;
              }

              // let materials = dataMaterial
              //   .map((e) => {
              //     return e.Material;
              //   })
              //   .filter(onlyUnique)
              //   .map((e, i) => {
              //     return {
              //       key: i,
              //       value: e,
              //     };
              //   });
              // this.getView()
              //   .byId('filterMaterial')
              //   .setModel(
              //     new sap.ui.model.json.JSONModel({
              //       FilterSet: materials,
              //     })
              //   );

              // let compMaterials = dataMaterial
              //   .map((e) => {
              //     return e.MaterialType;
              //   })
              //   .filter(onlyUnique)
              //   .map((e, i) => {
              //     return {
              //       key: i,
              //       value: e,
              //     };
              //   });
              // this.getView()
              //   .byId('filterComponentMaterial')
              //   .setModel(
              //     new sap.ui.model.json.JSONModel({
              //       FilterSet: compMaterials,
              //     })
              //   );
              //////

            },
            error: function (err) {
              sap.ui.core.BusyIndicator.hide();
              const details =
                (err && err.message && err.message.value) ||
                'Error not defined.';
              MessageBox.error('Error', {
                title: 'Error found searching',
                details,
              });
            },
          });
        },
        onExit: function () {
          // to destroy templates for bound aggregations when templateShareable is true on exit to prevent duplicateId issue
          var aControls = [
            {
              controlId:
                'Fiori_ListReport_ListReport_0-content-Fiori_ListReport_Table-1',
              groups: ['items'],
            },
          ];
          for (var i = 0; i < aControls.length; i++) {
            var oControl = this.getView().byId(aControls[i].controlId);
            if (oControl) {
              for (var j = 0; j < aControls[i].groups.length; j++) {
                var sAggregationName = aControls[i].groups[j];
                var oBindingInfo = oControl.getBindingInfo(sAggregationName);
                if (oBindingInfo) {
                  var oTemplate = oBindingInfo.template;
                  oTemplate.destroy();
                }
              }
            }
          }
        },
      }
    );
  },
  /* bExport= */ true
);
